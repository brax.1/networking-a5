import java.util.*;
import java.net.*;
import javax.net.ssl.*;
import java.io.*;
import javax.net.*;

// ** https://docs.oracle.com/javase/7/docs/api/java/net/MulticastSocket.html **//
// ** https://www.baeldung.com/java-broadcast-multicast **//

public class ChatClient extends Thread{

	public void run(){
		try{
			//declaring varliables
			int port = 40225;
			InetAddress group = InetAddress.getByName("239.0.202.1");
			String message;
			String recieved;
			Scanner scanner = new Scanner(System.in);
			byte[] buf = new byte[1500];

			//recieving a message
			while(true){

			//creating the multicast socket
 			MulticastSocket s = new MulticastSocket(port);
 			s.joinGroup(group);
			
			//trying to send a message
			//System.out.println("Enter Message to send");
			message = scanner.nextLine();
			DatagramPacket packet = new DatagramPacket(message.getBytes(), message.length(), group, port);
			s.send(packet);


			DatagramPacket dp = new DatagramPacket(buf, buf.length);
			s.receive(dp);
			recieved = new String(dp.getData(), 0 , dp.getLength());
			InetAddress senderAddress = dp.getAddress();
			String hostName = senderAddress.getHostAddress();
			System.out.println(hostName + ": " + recieved);
			}
			//s.close();
			//s.leaveGroup(group);
		} catch(Exception e){System.out.println("error: " + e);}
	}

	public static void main(String args[]){
			ChatClient thread = new ChatClient();
		try{
			//while(true){
			thread.start();
			//}
		}
		catch(Exception e){System.out.println(e);}
	}

}


